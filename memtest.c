#include "types.h"
#include "user.h"
#define PGSIZE 4096

#define ASSERT(cond, ...) \
    if(!cond){ \
      printf(2, "FAIL at %s:%d - ", __FUNCTION__, __LINE__); \
      printf(2, "\n"); \
      exit(); \
    }

int do_malloc() {
    char * a = malloc(100 * PGSIZE);
    free(a);
    return 0;
}

// Allocate a large chunk of memory and write to it
int multiple_pgdir_test() {
    printf(1,"multiple pgdir test\n");
    int i;
    int * p = malloc(10 * 1024 * PGSIZE);
    for(i=0 ; i<10 ; i++){
        printf(1, "writing to %d\n", p+i*PGSIZE + 100);
        p[i*PGSIZE + 100] = i;
        p[i*PGSIZE + 200] = i;
    }
    free(p);
    return 0;
}

int fork_test() {
    #define ARR_SIZE 5
    printf(1,"fork test test\n");
    int numOfProcs = 10;
    int i = 0;
    int j = 0;
    int pid;
    printf(1, "calling malloc\n");
    int * arr = malloc(4 * ARR_SIZE);
    for (i = 0; i < ARR_SIZE; i++) {
        arr[i] = i;
    }

    for (i = 0; i < numOfProcs; i++) {
        printf(1, "forking: %d\n", i);
        if ((pid = fork()) == 0) {
            for (j = 0; j < ARR_SIZE; j++) {
                ASSERT(arr[j] == j);
            }
            exit();
        } else {
            ASSERT(wait() == pid);
        }
    }
    return 0;
}

void recursion(int n) {
    int i = 1;
    printf(1, "%d\n", n);
    i++;
    recursion(n + 1);
}

int stack_overflow() {
    printf(1, "stack overflow test\n");
    int pid;
    if ((pid = fork()) == 0) {
        recursion(0);
        printf(1,"\n\n Should not got in here\n\n");
    } else {
        ASSERT(wait() == pid);
    }
    return 0;
}

int malloc_to_kernel() {
    printf(1,"malloc to kernel test\n");
    unsigned int gb = 1024 * 1024 * 1024;
    ASSERT(malloc(gb) != 0);
    ASSERT(malloc(gb + 100) == 0);

    return 0;
}

int
main(int argc, char *argv[]) {
    printf(1, "\n\nDon't forget to enable lazy memory allocation prints in trap.c!!\n\n");
    do_malloc();
    multiple_pgdir_test();
    fork_test();
    stack_overflow();
    malloc_to_kernel();
    exit();
}

